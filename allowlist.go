package main

import (
	"fmt"
	"log"

	"vitess.io/vitess/go/vt/sqlparser"

	"github.com/pingcap/tidb/parser"
	"github.com/pingcap/tidb/parser/ast"
	_ "github.com/pingcap/tidb/parser/test_driver"
)

const (
	PermissionReadonly = "readonly"
	PermissionModify   = "modify"
	PermissionUnknown  = "unknown"
)

type TablePermission struct {
	Permissions []string
	Columns     []string
}

var Allowlist = map[string]TablePermission{
	"information_schema.leaves": {
		Permissions: []string{PermissionReadonly, PermissionModify},
		Columns:     []string{ColumnHost, ColumnPort, ColumnAvailabilityGroup, ColumnPairHost, ColumnPairPort},
	},
	"information_schema.distributed_partitions": {
		Permissions: []string{PermissionReadonly},
		Columns:     []string{ColumnDatabaseName, ColumnOrdinal},
	},
}

var TablePermissions = map[string][]string{
	"leaves":                         {PermissionReadonly, PermissionModify},
	"distributed_partitions":         {PermissionReadonly},
	"distributed_databases_on_sites": {PermissionReadonly},
	"mv_bottomless_status_extended":  {PermissionReadonly},
	"mv_processlist":                 {PermissionReadonly},
	"mv_nodes":                       {PermissionReadonly},
}

var Queries = []string{
	`select * from information_schema.mv_bottomless_status_extended where database_name='dashboard_service' and ordinal = 1 and role = 'master'`,

	`SELECT ps.NODE_ID, ps.ID, ps.STATE, ps.TIME, ps.INFO
	FROM
	information_schema.MV_PROCESSLIST ps
	JOIN information_schema.MV_NODES n ON ps.NODE_ID = n.ID
	WHERE
	COMMAND != 'Sleep'
	ORDER BY ps.TIME DESC`,

	`Select * from information_schema.users_groups where "GROUP" like '%C%'`,

	`DROP TABLE IF EXISTS INFORMATION_SCHEMA.DISTRIBUTED_DATABASES_ON_SITES;`,

	`DELETE FROM information_schema.leaves WHERE NODE_ID = ?;`,

	`UPDATE INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS SET IS_OFFLINE = 1 WHERE DATABASE_NAME = ? AND ORDINAL = ?;`,

	`INSERT INTO INFORMATION_SCHEMA.DISTRIBUTED_DATABASES_ON_SITES (DATABASE_NAME, SITE_NAME, TYPE, STORAGE_ID, COMPUTE_ID) VALUES (?, ?, ?, ?, ?);`,

	`SELECT
		COUNT(CASE WHEN ROLE = 'Master' THEN 1 END) as masterCount,
		COUNT(CASE WHEN ROLE = 'Slave' THEN 1 END) as slaveCount,
		INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS.DATABASE_NAME  as databaseName,
		INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS.ORDINAL as ordinal
	FROM INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS
	LEFT JOIN INFORMATION_SCHEMA.DISTRIBUTED_DATABASES_ON_SITES
		ON INFORMATION_SCHEMA.DISTRIBUTED_DATABASES_ON_SITES.DATABASE_NAME  =
			INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS.DATABASE_NAME 
	WHERE INFORMATION_SCHEMA.DISTRIBUTED_DATABASES_ON_SITES.SITE_NAME = ?
		AND INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS.IS_READ_REPLICA = 0
		AND INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS.IS_OFFLINE = 0
		AND INFORMATION_SCHEMA.DISTRIBUTED_DATABASES_ON_SITES.TYPE != 'READONLY'
	GROUP BY
		databaseName,
		INFORMATION_SCHEMA.DISTRIBUTED_PARTITIONS.ORDINAL`,
}

const (
	TableLeaves                      = "information_schema.leaves"
	TableDistributedPartitions       = "information_schema.distributed_partitions"
	TableDistributedDatabasesOnSites = "information_schema.distributed_databases_on_sites"
	TableMvBottomlessStatusExtended  = "information_schema.mv_bottomless_status_extended"
	TableMvProcesslist               = "information_schema.MV_PROCESSLIST"
	TableMvNodes                     = "information_schema.MV_NODES"
	TableUsersGroups                 = "information_schema.users_groups"

	ColumnHost                    = "HOST"
	ColumnPort                    = "PORT"
	ColumnAvailabilityGroup       = "AVAILABILITY_GROUP"
	ColumnPairHost                = "PAIR_HOST"
	ColumnPairPort                = "PAIR_PORT"
	ColumnState                   = "STATE"
	ColumnOpenedConnections       = "OPENED_CONNECTIONS"
	ColumnAverageRoundtripLatency = "AVERAGE_ROUNDTRIP_LATENCY"
	ColumnNodeId                  = "NODE_ID"
	ColumnGracePeriodInSeconds    = "GRACE_PERIOD_IN_SECONDS"
	ColumnMinimumPrePrepareTs     = "MINIMUM_PRE_PREPARE_TS"
	ColumnRuntimeState            = "RUNTIME_STATE"
	ColumnDatabaseId              = "DATABASE_ID"
	ColumnDatabaseName            = "DATABASE_NAME"
	ColumnOrdinal                 = "ORDINAL"
	ColumnRole                    = "ROLE"
	ColumnLocked                  = "LOCKED"
	ColumnIsOffline               = "IS_OFFLINE"
	ColumnSyncDesired             = "SYNC_DESIRED"
	ColumnTerm                    = "TERM"
	ColumnInstanceId              = "INSTANCE_ID"
	ColumnIsReadReplica           = "IS_READ_REPLICA"
	ColumnSiteId                  = "SITE_ID"
	ColumnSafeTs                  = "SAFE_TS"
	ColumnReadReplicaState        = "READ_REPLICA_STATE"
	ColumnSiteName                = "SITE_NAME"
	ColumnType                    = "TYPE"
	ColumnStorageId               = "STORAGE_ID"
	ColumnComputeId               = "COMPUTE_ID"
)

// Function to classify the query type using sqlparser and extract table names
func classifyQuery(query string, parser *sqlparser.Parser) (string, []string) {
	stmt, err := parser.Parse(query)
	if err != nil {
		log.Printf("Error parsing query: %v", err)
		return "unknown", nil
	}
	switch stmt := stmt.(type) {
	case *sqlparser.Select, *sqlparser.Show:
		tables, database := extractTablesAndDatabase(stmt)
		fmt.Println("database is ", database)
		return PermissionReadonly, tables
	case *sqlparser.Insert, *sqlparser.Update, *sqlparser.Delete, *sqlparser.AlterDatabase, *sqlparser.AlterTable, *sqlparser.CreateDatabase, *sqlparser.CreateTable, *sqlparser.DropDatabase, *sqlparser.DropTable, *sqlparser.RenameTable, *sqlparser.TruncateTable, *sqlparser.Set, *sqlparser.Use, *sqlparser.Begin, *sqlparser.Commit, *sqlparser.Rollback, *sqlparser.Savepoint, *sqlparser.Release, *sqlparser.LockTables, *sqlparser.UnlockTables:
		tables, database := extractTablesAndDatabase(stmt)
		fmt.Println("database is ", database)

		return PermissionModify, tables
	default:
		log.Printf("Unknown statement type: %T", stmt)
		return PermissionUnknown, nil
	}
}

// Function to extract table names and database name from a statement
func extractTablesAndDatabase(stmt sqlparser.Statement) ([]string, string) {
	var tables []string
	var database string

	// Helper function to add a table name to the list
	addTable := func(table sqlparser.TableName) {
		if table.Name.String() != "" {
			tables = append(tables, table.Name.String())
		}
		if table.Qualifier.String() != "" {
			database = table.Qualifier.String()
		}
	}

	// Walk through the statement and extract table names
	sqlparser.Walk(func(node sqlparser.SQLNode) (kontinue bool, err error) {
		switch node := node.(type) {
		case *sqlparser.AliasedTableExpr:
			if tbl, ok := node.Expr.(sqlparser.TableName); ok {
				addTable(tbl)
			}
		case *sqlparser.TableName:
			addTable(*node)
		case *sqlparser.JoinTableExpr:
			if leftExpr, ok := node.LeftExpr.(*sqlparser.AliasedTableExpr); ok {
				if tbl, ok := leftExpr.Expr.(sqlparser.TableName); ok {
					addTable(tbl)
				}
			}
			if rightExpr, ok := node.RightExpr.(*sqlparser.AliasedTableExpr); ok {
				if tbl, ok := rightExpr.Expr.(sqlparser.TableName); ok {
					addTable(tbl)
				}
			}
		}
		return true, nil
	}, stmt)

	return tables, database
}

// Function to check if a query is allowed on a table
func validateQueryPermission(table string, queryType string) bool {
	permissions, ok := TablePermissions[table]
	// fmt.Printf("Table: %s, QueryType: %s, Permission: %v\n", table, queryType, permissions)

	if !ok {
		log.Printf("Table not found in permissions: %s", table)
		return false
	}
	for _, permission := range permissions {
		if permission == queryType {
			return true
		}
	}
	return false
}

func main() {
	// // fmt.Printf("%v", TablePermissions)
	// fmt.Printf("%v", TablePermissions["leaves"])

	// opts := sqlparser.Options{
	// 	MySQLServerVersion: "5.7",
	// 	TruncateErrLen:     0,
	// 	TruncateUILen:      512,
	// }

	// // create a new parser instance
	// parser, err := sqlparser.New(opts)
	// if err != nil {
	// 	log.Fatalf("Error creating parser: %v", err)
	// }

	// queries_arr := Queries

	// for _, query := range queries_arr {
	// 	queryType, tableNames := classifyQuery(query, parser)

	// 	fmt.Printf("Query type: %s\n", queryType)
	// 	fmt.Printf("tableNames: %s\n", tableNames)
	// 	for _, table := range tableNames {
	// 		tableName := strings.ToLower(table)
	// 		if !validateQueryPermission(tableName, queryType) {
	// 			fmt.Printf("Query not allowed on table: %s", table)
	// 		} else {
	// 			fmt.Printf("Query allowed on table: %s\n", table)
	// 		}
	// 	}
	// 	fmt.Printf("\n")

	// }

	for _, query := range Queries {
		Run(query)
		fmt.Println("\n")
	}
}

func Run(query string) {
	p := parser.New()

	sql := query
	stmtNodes, _, err := p.Parse(sql, "", "")
	if err != nil {
		fmt.Printf("parse error: %v\n", err.Error())
		return
	}

	for _, stmtNode := range stmtNodes {
		switch node := stmtNode.(type) {
		case *ast.SelectStmt:
			fmt.Println("Query type: SELECT")
			extractTableInfo(node)
		case *ast.InsertStmt:
			fmt.Println("Query type: INSERT")
			extractTableInfo(node)
		case *ast.UpdateStmt:
			fmt.Println("Query type: UPDATE")
			extractTableInfo(node)
		case *ast.DeleteStmt:
			fmt.Println("Query type: DELETE")
			extractTableInfo(node)
		default:
			fmt.Println("Unknown query type")
		}
	}
}

func extractTableInfo(node ast.Node) {
	tableVisitor := &tableNameVisitor{}
	node.Accept(tableVisitor)

	for _, tableName := range tableVisitor.tableNames {
		if tableName.Schema.O != "" {
			fmt.Printf("Database: %s, Table: %s\n", tableName.Schema.O, tableName.Name.O)
		} else {
			fmt.Printf("Table: %s\n", tableName.Name.O)
		}
	}
}

type tableNameVisitor struct {
	tableNames []*ast.TableName
}

func (v *tableNameVisitor) Enter(n ast.Node) (ast.Node, bool) {
	if tableName, ok := n.(*ast.TableName); ok {
		v.tableNames = append(v.tableNames, tableName)
	}
	return n, false
}

func (v *tableNameVisitor) Leave(n ast.Node) (ast.Node, bool) {
	return n, true
}
